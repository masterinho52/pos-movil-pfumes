import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { ProductsPage } from "./../pages/products/products";
import { ProductPage } from "./../pages/product/product";
import { WelcomePage } from "./../pages/welcome/welcome";
import { TabsPage } from "./../pages/tabs/tabs";
import { SignupPage } from "./../pages/signup/signup";
import { LoginPage } from "./../pages/login/login";

import { Proveedor1Provider } from "../providers/proveedor1/proveedor1";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { CartProvider } from "../providers/cart/cart";
import { HTTP } from "@ionic-native/http";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    WelcomePage,
    TabsPage,
    ProductsPage,
    ProductPage,
    SignupPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    // HTTP,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    WelcomePage,
    TabsPage,
    ProductsPage,
    ProductPage,
    SignupPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Proveedor1Provider,
    CartProvider
  ]
})
export class AppModule {}
