export class Item {
  id: any;
  item_name: string;
  description: string;
  selling_price: number;
  quantity: number;
  category: string;
  constructor() {}
}
