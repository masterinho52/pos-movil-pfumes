import { Component } from "@angular/core";
import { NavController, App, LoadingController } from "ionic-angular";
import { Proveedor1Provider } from "../../providers/proveedor1/proveedor1";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  userDetails: any;
  responseData: any;

  userPostData = { email: "", token: "", name: "" };

  constructor(
    public navCtrl: NavController,
    public app: App,
    public authService: Proveedor1Provider,
    public loadingCtrl: LoadingController
  ) {
    // console.log(" home - localstorage " + localStorage.getItem("userData"));
    const data = JSON.parse(localStorage.getItem("userData"));
    // console.log(" home - JSON PARSE  " + data);

    // const datos = JSON.stringify(localStorage.getItem("userData"));
    // console.log(" home - JSON STRINGIFY " + datos);

    // const data = localStorage.getItem("userData");
    this.userDetails = data;
    console.log(" home token parse " + this.userDetails.user.name);
    // this.responseData = datos;
    // console.log(" home token sringify " + this.responseData.token);
    // console.log(" home localstorage " + this.userDetails);
    this.userPostData.name = this.userDetails.user.name;
    this.userPostData.email = this.userDetails.user.email;
    this.userPostData.token = this.userDetails.token;
  }

  backToWelcome(loading) {
    loading.dismiss();
    const root = this.app.getRootNav();
    root.popToRoot();
  }

  logout() {
    let loading = this.loadingCtrl.create({ content: "Cerrando sesión" });
    loading.present();
    localStorage.clear();
    setTimeout(() => this.backToWelcome(loading), 1000);
  }

  // ionViewDidLoad(){
  //  this.proveedor.obtenerDatos()
  //  .subscribe(
  //    (data)=> { this.usuarios=data; },
  //    (error)=>{ console.log(error); }
  //  )
  // }
}
