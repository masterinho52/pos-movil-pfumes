import { Component } from "@angular/core";
import {
  // IonicPage,
  NavController,
  LoadingController,
  ToastController
} from "ionic-angular";

import { SignupPage } from "./../signup/signup";
import { TabsPage } from "./../tabs/tabs";
import { WelcomePage } from "./../welcome/welcome";
import { Proveedor1Provider } from "./../../providers/proveedor1/proveedor1";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  responseData: any;
  userData = {
    password: "",
    email: ""
  };

  constructor(
    public navCtrl: NavController,
    public authService: Proveedor1Provider,
    public loadingCtrl: LoadingController,
    public toastCtl: ToastController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
  }

  login() {
    let loading = this.loadingCtrl.create({ content: "Validando usuario" });
    loading.present();
    if (this.userData.password && this.userData.email) {
      // console.log(
      //   " datos " + this.userData.password + " " + this.userData.email
      // );
      this.authService.postData(this.userData, "login").then(
        result => {
          loading.dismiss();
          this.responseData = result;
          this.responseData = JSON.parse(this.responseData);
          // this.mostrarNotificacion(
          //   "Respuesta " + this.responseData.token,
          //   "middle"
          // );
          // console.log(" respuesta " + this.responseData);
          // console.log(
          //   " respuesta token " + JSON.parse(this.responseData.token)
          // );
          if (this.responseData) {
            // console.log(this.responseData);
            localStorage.setItem("userData", JSON.stringify(this.responseData));
            // console.log(
            //   " Login localstorage " + localStorage.getItem("userData")
            // );
            this.navCtrl.push(TabsPage, {}, { animate: false });
          } else {
            loading.dismiss();
            this.mostrarNotificacion(
              "Por favor ingrese información válida",
              "middle"
            );
            console.log("Credenciales incorrectas");
          }
        },
        err => {
          loading.dismiss();
          this.mostrarNotificacion("Credenciales incorrectas" + err, "bottom");
        }
      );
    } else {
      loading.dismiss();
      this.mostrarNotificacion(
        "Por favor ingrese información válida",
        "middle"
      );
      console.log("Ingrese información válida.");
    }
  }

  signup() {
    //Login page link
    this.navCtrl.push(SignupPage);
  }

  home() {
    //Login page link
    this.navCtrl.push(WelcomePage);
  }

  mostrarNotificacion(mensaje: string, posicion: string) {
    const toast = this.toastCtl.create({
      message: mensaje,
      position: posicion,
      // showCloseButton: true,
      // closeButtonText: "OK"
      duration: 4000
    });
    toast.present();
  }
}
