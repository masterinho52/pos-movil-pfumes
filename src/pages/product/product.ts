import { ProductsPage } from "./../products/products";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController
} from "ionic-angular";
import { Proveedor1Provider } from "./../../providers/proveedor1/proveedor1";
/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: "page-product",
  templateUrl: "product.html"
})
export class ProductPage {
  getProduct: {
    id: "";
    item_name: "";
    description: "";
    selling_price: "";
    quantity: "";
    category: "";
  };
  selectProduct: {
    item_name: "";
    description: "";
    selling_price: "";
    quantity: "";
    category: "";
  };
  productId: any;
  productCount: number = 1;
  cartItems: any[];
  title: string;
  userDetails: any;
  encabezado = {
    "Content-Type": "application/json",
    Authorization: ""
  };
  responseData: any;
  categorias = [
    { id: 1, name: "SUMINISTROS" },
    { id: 2, name: "ALMACENAMIENTO" },
    { id: 3, name: "COMPUTADORAS" },
    { id: 4, name: "ACCESORIOS" },
    { id: 5, name: "OTROS" }
  ];
  categoria_list: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: Proveedor1Provider,
    public loadingCtrl: LoadingController,
    public toastCtl: ToastController
  ) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data;
    this.encabezado.Authorization = "Bearer " + this.userDetails.token;
    this.title = this.navParams.get("title");
    if (this.navParams.get("item")) {
      this.selectProduct = this.navParams.get("item");
      this.getProduct = this.navParams.get("item");
      // window.localStorage.setItem(
      //   "selectedProduct",
      //   JSON.stringify(this.navParams.get("item"))
      // );
      window.localStorage.setItem(
        "title",
        JSON.stringify(this.navParams.get("title"))
      );

      this.productId = this.getProduct.id;
    } else {
      this.selectProduct = {
        item_name: "",
        description: "",
        selling_price: "",
        quantity: "",
        category: ""
      };
    }
    this.categoria_list = this.categorias;
    // console.log("categorias " + JSON.stringify(this.categorias));
    // console.log("categorias[1] " + this.categorias[1]);
  }

  inicializar() {
    // this.getProduct = JSON.parse(
    //   window.localStorage.getItem("selectedProduct")
    // );
    // this.selectProduct.item_name = this.getProduct.item_name;
    // this.selectProduct.description = this.getProduct.description;
    // this.selectProduct.selling_price = this.getProduct.selling_price;
    // this.selectProduct.quantity = this.getProduct.quantity;
    // this.selectProduct.category = this.getProduct.category;
    this.productId = this.getProduct.id;
    this.title = this.navParams.get("title");
    console.log(this.selectProduct);
  }

  guardar() {
    console.log("entreo a guardar");
    let loading = this.loadingCtrl.create({ content: "Guardando datos..." });
    loading.present();
    if (
      this.selectProduct.item_name &&
      this.selectProduct.description &&
      this.selectProduct.quantity &&
      this.selectProduct.selling_price
    ) {
      // console.log(
      //   " datos " + this.userData.password + " " + this.userData.email
      // );
      if (this.title == "Editar") {
        this.authService
          .putDataHeaders(
            JSON.stringify(this.selectProduct),
            "items/" + this.productId,
            this.encabezado
          )
          .then(
            result => {
              // loading.dismiss();
              this.responseData = result;
              this.responseData = JSON.parse(this.responseData);
              console.log(
                " respuesta completa1 " + JSON.stringify(this.responseData)
              );
              console.log(" respuesta completa2 " + this.responseData);
              // console.log(
              //   " respuesta result " + this.responseData.result)
              // );
              loading.dismiss();
              if (this.responseData.result) {
                console.log(this.responseData.result);
                this.mostrarNotificacion(
                  "Producto guardado exitosamente",
                  "bottom"
                );
              } else {
                loading.dismiss();
                this.mostrarNotificacion("Error al guardar datos", "bottom");
                console.log(JSON.stringify(this.responseData));
              }
            },
            err => {
              loading.dismiss();
              this.mostrarNotificacion("Error al guardar datos", "bottom");
              console.log(JSON.stringify(err));
            }
          );
      } /* Nuevo */ else {
        this.authService
          .postDataHeaders(
            JSON.stringify(this.selectProduct),
            "items",
            this.encabezado
          )
          .then(
            result => {
              // loading.dismiss();
              this.responseData = result;
              this.responseData = JSON.parse(this.responseData);
              console.log(
                " respuesta completa1 " + JSON.stringify(this.responseData)
              );
              console.log(" respuesta completa2 " + this.responseData);
              console.log(
                " respuesta result " + JSON.stringify(this.responseData.result)
              );
              loading.dismiss();
              if (this.responseData.result) {
                console.log(this.responseData.result);
                this.mostrarNotificacion(
                  "Producto guardado exitosamente",
                  "bottom"
                );
              } else {
                loading.dismiss();
                this.mostrarNotificacion("Error al guardar datos", "bottom");
                console.log(JSON.stringify(this.responseData));
              }
            },
            err => {
              loading.dismiss();
              this.mostrarNotificacion(
                "Error al guardar datos" + err,
                "bottom"
              );
              console.log(JSON.stringify(this.responseData));
            }
          );
      }
    } else {
      loading.dismiss();
      console.log(
        this.selectProduct.item_name +
          " - " +
          this.selectProduct.description +
          " - " +
          this.selectProduct.quantity +
          " - " +
          this.selectProduct.selling_price
      );
      this.mostrarNotificacion(
        "Por favor ingrese información válida",
        "bottom"
      );
      console.log("Ingrese información válida.");
    }
  }

  mostrarNotificacion(mensaje: string, posicion: string) {
    const toast = this.toastCtl.create({
      message: mensaje,
      position: posicion,
      duration: 4000
    });
    toast.present();
  }

  mostrarNotificacionBoton(mensaje: string, posicion: string) {
    const toast = this.toastCtl.create({
      message: mensaje,
      position: posicion,
      showCloseButton: true,
      closeButtonText: "OK"
    });
    toast.onDidDismiss(this.dismissHandler);
    toast.present();
  }

  private dismissHandler() {
    // this.navCtrl.push(ProductsPage, {}, { animate: false });
    console.log("index " + this.navCtrl.indexOf);
    console.log("getViews" + this.navCtrl.getViews);
  }
}
