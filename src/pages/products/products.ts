import { Component } from "@angular/core";
import {
  // IonicPage,
  NavController,
  LoadingController,
  ToastController,
  AlertController
} from "ionic-angular";

import { Proveedor1Provider } from "./../../providers/proveedor1/proveedor1";
import { ProductPage } from "./../product/product";
/**
 * Generated class for the ProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: "page-products",
  templateUrl: "products.html"
})
export class ProductsPage {
  userDetails: any;
  userPostdata = {
    password: "",
    email: ""
  };
  userData = {};
  // responseData = { email: "", name: "" };
  responseData: any;
  datosLegibles: any;
  items: any;

  encabezado = {
    "Content-Type": "application/json",
    Authorization: ""
  };

  constructor(
    public navCtrl: NavController,
    public authService: Proveedor1Provider,
    public loadingCtrl: LoadingController,
    public toastCtl: ToastController,
    private alertCtrl: AlertController
  ) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.userDetails = data;
    this.encabezado.Authorization = "Bearer " + this.userDetails.token;
  }

  ionViewWillEnter() {
    console.log("ionViewDidLoad ProductsPage");
    this.getItems();
  }

  getItems() {
    let loading = this.loadingCtrl.create({
      content: "Obteniendo información"
    });
    loading.present();
    if (this.encabezado.Authorization) {
      console.log(
        " datos " +
          JSON.stringify(this.userData) +
          " Header: " +
          this.encabezado.Authorization
      );
      this.authService
        .getDataHeaders("items", this.encabezado, this.userData)
        .then(
          result => {
            loading.dismiss();
            this.responseData = result;
            // console.log("data antes " + this.responseData);
            this.responseData = JSON.parse(this.responseData);
            // console.log("data despues " + this.responseData);
            // this.mostrarNotificacion(
            //   "Respuesta " + this.responseData,
            //   "middle"
            // );

            // this.datosLegibles = JSON.stringify(this.responseData.item);
            this.datosLegibles = this.responseData.item;
            this.items = this.datosLegibles;
            console.log(this.datosLegibles);
          },
          err => {
            loading.dismiss();
            console.log("error del result ");
            this.mostrarNotificacion("Error en el result ", "middle");
          }
        );
    } else {
      loading.dismiss();
      this.mostrarNotificacion("Error al obtener información", "middle");
      console.log("Ingrese información válida.");
    }
  }

  serachItems(ev) {
    // Reset items back to all of the items
    // this.getItems();

    // set val to the value of the ev target
    var val = ev.target.value;
    console.log("parametro busqueda " + val);

    // if the value is an empty string don't filter the items
    if (val && val.trim() != "") {
      this.datosLegibles = this.datosLegibles.filter(item => {
        console.log("item" + item);
        return item.item_name.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.datosLegibles = this.items;
    }
  }
  editProduct(item) {
    this.navCtrl.push(ProductPage, { item: item, title: "Editar" });
  }

  createProduct() {
    this.navCtrl.push(ProductPage, { title: "Nuevo" });
  }
  mostrarNotificacion(mensaje: string, posicion: string) {
    const toast = this.toastCtl.create({
      message: mensaje,
      position: posicion,
      duration: 4000
    });
    toast.present();
  }
  eliminar(item) {
    this.presentConfirm(
      "Confirmación",
      "Desea eliminar el articulo: " + item.item_name + "?",
      item
    );
  }

  presentConfirm(titulo, mensaje, item) {
    let alert = this.alertCtrl.create({
      title: titulo,
      message: mensaje,
      buttons: [
        {
          text: "Cancelar",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Aceptar",
          handler: () => {
            console.log("Aceptar clicked");
            this.deleteItem(item);
          }
        }
      ]
    });
    alert.present();
  }

  deleteItem(item) {
    let loading = this.loadingCtrl.create({
      content: "Eliminando..."
    });
    loading.present();
    if (this.encabezado.Authorization) {
      console.log(
        " datos " + this.userData + " Header: " + this.encabezado.Authorization
      );
      this.authService.deleteDataHeaders("items/" + item.id, {}, {}).then(
        result => {
          loading.dismiss();
          this.getItems();
          this.mostrarNotificacion("Producto eliminado", "bottom");

          // this.datosLegibles = JSON.stringify(this.responseData.item);
        },
        err => {
          console.log("Error al borrar " + err);
          this.mostrarNotificacion(
            "Error al eliminar, por favor intente mas tarde",
            "middle"
          );
        }
      );
    } else {
      loading.dismiss();
      this.mostrarNotificacion(
        "Error al eliminar, por favor intente mas tarde",
        "middle"
      );
    }
  }
}
