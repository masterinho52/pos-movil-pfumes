import { Component } from "@angular/core";
import {
  // IonicPage,
  NavController,
  LoadingController,
  ToastController
} from "ionic-angular";

import { LoginPage } from "./../login/login";
import { TabsPage } from "./../tabs/tabs";
import { Proveedor1Provider } from "./../../providers/proveedor1/proveedor1";
import { WelcomePage } from "../welcome/welcome";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  responseData: any;
  // userData = {"token": "",
  //   "user": {
  //       "name": "",
  //       "email": ""
  //     } };
  userData = {
    name: "",
    email: "",
    password: "",
    confirm_password: ""
  };

  constructor(
    public navCtrl: NavController,
    public authService: Proveedor1Provider,
    public loadingCtrl: LoadingController,
    public toastCtl: ToastController
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad SignupPage");
  }

  signup() {
    let loading = this.loadingCtrl.create({ content: "Registrando usuario.." });
    loading.present();
    if (
      this.userData.name &&
      this.userData.password &&
      this.userData.email &&
      this.userData.confirm_password
    ) {
      console.log("Antes llamada");
      this.authService.postData(this.userData, "register").then(
        result => {
          loading.dismiss();
          this.responseData = result;
          // console.log("Resultado " + result);
          if (this.responseData.token) {
            console.log(this.responseData);
            localStorage.setItem("userData", JSON.stringify(this.responseData));
            this.navCtrl.push(TabsPage);
          } else {
            console.log("User already exists");
          }
        },
        err => {
          // Error log
        }
      );
    } else {
      loading.dismiss();
      this.mostrarNotificacion(
        "Por favor ingrese información válida",
        "middle"
      );
      console.log("Ingrese información válida.");
    }
  }

  login() {
    //Login page link
    this.navCtrl.push(LoginPage);
  }
  home() {
    //Login page link
    this.navCtrl.push(WelcomePage);
  }

  mostrarNotificacion(mensaje: string, posicion: string) {
    const toast = this.toastCtl.create({
      message: mensaje,
      position: posicion,
      duration: 4000
    });
    toast.present();
  }
}
