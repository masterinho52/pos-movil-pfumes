import { SalesPage } from "./../sales/sales";
import { ProductsPage } from "./../products/products";
import { HomePage } from "./../home/home";
import { Component } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: "page-tabs",
  templateUrl: "tabs.html"
})
export class TabsPage {
  tab1Root = HomePage;
  tab2Root = ProductsPage;
  tab3Root = SalesPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad TabsPage");
  }
}
