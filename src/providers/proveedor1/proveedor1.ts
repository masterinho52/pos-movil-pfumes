import { Injectable } from "@angular/core";
import {
  Http,
  Headers,
  RequestOptionsArgs,
  RequestOptions
} from "@angular/http";
import "rxjs/add/operator/map";
// import { HTTP } from "@ionic-native/http";
import { HTTP, HTTPResponse } from "@ionic-native/http";

/*
  Generated class for the Proveedor1Provider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
// let headers = new Headers();
//     headers.append('Content-Type', 'application/json');
// let aipUrl = "http://apiumesmf.local/api/";
// let aipUrl = "/api/";
let aipUrl = "http://pfinaljramirez.cacao.gt/api/";

@Injectable()
export class Proveedor1Provider {
  // aipUrl='https://jsonplaceholder.typicode.com/users';
  constructor(public http1: Http, private http: HTTP) {
    console.log("Hello Proveedor1Provider Provider");
  }

  postData(credentials, type) {
    var headers = new Headers();
    headers.append("Content-Type", "application/json");
    let options = new RequestOptions({
      headers: headers
    });
    return new Promise((resolve, reject) => {
      // nueva forma  inicio
      this.http
        .post(aipUrl + type, credentials, {
          "Content-Type": "application/json"
        })

        .then(data => {
          resolve(data.data);
        })
        .catch(error => {
          reject(error.error);
        });
      // nueva forma fin
    });
  }

  // postData(credentials, type) {
  //   return new Promise((resolve, reject) => {
  //     let headers = new Headers({
  //       "Content-Type": "application/json"
  //     });

  //     this.http
  //       .post(aipUrl + type, JSON.stringify(credentials), { headers: headers })

  //       .subscribe(
  //         res => {
  //           resolve(res.json());
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }

  // postDataHeaders(credentials, type, encabezados) {
  //   return new Promise((resolve, reject) => {
  //     let headers = new Headers(encabezados);

  //     this.http
  //       .post(aipUrl + type, JSON.stringify(credentials), { headers: headers })
  //       .subscribe(
  //         res => {
  //           resolve(res.json());
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }

  postDataHeaders(credentials, type, encabezados) {
    var headers = new Headers(encabezados);
    let options = new RequestOptions({
      headers: headers
    });

    return new Promise((resolve, reject) => {
      // nueva forma inicio
      this.http
        .post(aipUrl + type, credentials, {})

        .then(data => {
          resolve(data.data);
        })
        .catch(error => {
          reject(error.error);
        });
      // nueva forma fin
    });
  }

  putDataHeaders(parameters, type, encabezados) {
    return new Promise((resolve, reject) => {
      let headers = new Headers(encabezados);
      // nueva forma inicio
      this.http
        .put(aipUrl + type, parameters, {})

        .then(data => {
          resolve(data.data);
        })
        .catch(error => {
          reject(error.error);
        });
      // nueva forma fin
      // this.http
      //   .put(aipUrl + type, JSON.stringify(parameters), {
      //     headers: headers
      //   })
      //   .then(val => {
      //     resolve(val.data.json());
      //   })
      //   .catch(e => {
      //     reject(e);
      //   });
    });
  }

  // putDataHeaders(parameters, type, encabezados) {
  //   return new Promise((resolve, reject) => {
  //     let headers = new Headers(encabezados);

  //     this.http
  //       .put(aipUrl + type, JSON.stringify(parameters), {
  //         headers: headers
  //       })
  //       .subscribe(
  //         res => {
  //           resolve(res.json());
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }

  // getDataHeaders(type, encabezados, parametros) {
  //   return new Promise((resolve, reject) => {
  //     let myHeaders = new Headers(encabezados);

  //     let myParams = new URLSearchParams(parametros);
  //     // myParams.append("id", bookId);
  //     let options = new RequestOptions({
  //       headers: myHeaders,
  //       params: myParams
  //     });

  //     this.http
  //       .get(aipUrl + type, options)

  //       .subscribe(
  //         res => {
  //           resolve(res.json());
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }

  getDataHeaders(type, encabezados, parametros) {
    return new Promise((resolve, reject) => {
      let myHeaders = new Headers(encabezados);

      let myParams = new URLSearchParams(parametros);
      // myParams.append("id", bookId);
      let options = new RequestOptions({
        headers: myHeaders,
        params: myParams
      });
      // nueva forma inicio
      this.http
        .get(aipUrl + type, {}, {})
        .then(data => {
          resolve(data.data);
        })
        .catch(error => {
          reject(error.error);
        });
      // nueva forma fin
    });
  }

  deleteDataHeaders(type, encabezados, parametros) {
    return new Promise((resolve, reject) => {
      let myHeaders = new Headers(encabezados);

      let myParams = new URLSearchParams(parametros);
      // myParams.append("id", bookId);
      let options = new RequestOptions({
        headers: myHeaders,
        params: myParams
      });

      // nueva forma inicio
      this.http
        .delete(aipUrl + type, parametros, {})

        .then(data => {
          resolve(data.data);
        })
        .catch(error => {
          reject(error.error);
        });
      // nueva forma fin

      // this.http
      //   .delete(aipUrl + type, parametros, { headers: myHeaders })

      //   .then(val => {
      //     resolve(val.data.json());
      //   })
      //   .catch(e => {
      //     reject(e);
      //   });
    });
  }

  // deleteDataHeaders(type, encabezados, parametros) {
  //   return new Promise((resolve, reject) => {
  //     let myHeaders = new Headers(encabezados);

  //     let myParams = new URLSearchParams(parametros);
  //     // myParams.append("id", bookId);
  //     let options = new RequestOptions({
  //       headers: myHeaders,
  //       params: myParams
  //     });

  //     this.http
  //       .delete(aipUrl + type, options)

  //       .subscribe(
  //         res => {
  //           resolve(res.json());
  //         },
  //         err => {
  //           reject(err);
  //         }
  //       );
  //   });
  // }
}
